package com.example.protek.repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.example.protek.entity.CustomerDeatils;

@Repository

@Transactional
public interface CustomerDeatilsRepo  extends 
 JpaRepository<CustomerDeatils, Long>,

JpaSpecificationExecutor<CustomerDeatils>{

}
