package com.example.protek.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.example.protek.entity.CustomerPrice;


@Repository
public interface CustomerPriceRepo extends JpaRepository<CustomerPrice, Long>,JpaSpecificationExecutor<CustomerPrice>{

}
