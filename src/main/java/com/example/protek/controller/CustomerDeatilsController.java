package com.example.protek.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.protek.entity.CustomerDeatils;
import com.example.protek.service.CustomerDeatilsService;

@RestController
@RequestMapping("rest/api/")
public class CustomerDeatilsController {
	
@Autowired	
private CustomerDeatilsService cds;


@GetMapping("getbyid/{id}")
public ResponseEntity<Object> getDataById( @PathVariable Long id) {
	return cds.getDataByID(id);
}
	
@PostMapping("save")
public ResponseEntity<Object> saveD(@RequestBody CustomerDeatils cd) throws CloneNotSupportedException{
	return cds.saveD(cd);
}
	


}
