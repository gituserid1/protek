//package com.example.protek.controller.readexcelfile;
////
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.util.Iterator;
//
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("rest/api/excel")
//public class ExcelController {
//
//	@GetMapping("excel")
//	public ResponseEntity<Object> readExcelValaue() throws IOException{
//		
//		 // Try block to check fo exceptions
//        try {
// 
//            // Reading file fro local directory
//            FileInputStream file = new FileInputStream(
//                new File("gfgcontribute.xlsx"));
// 
//            // Create Workbook instance holding reference to
//            // .xlsx file
//            XSSFWorkbook workbook = new XSSFWorkbook(file);
// 
//            // Get first/desired sheet from the workbook
//            XSSFSheet sheet = workbook.getSheetAt(0);
// 
//            // Iterate through each rows one by one
//            Iterator<Row> rowIterator = sheet.iterator();
// 
//            // Till there is an element condition holds true
//            while (rowIterator.hasNext()) {
// 
//                Row row = rowIterator.next();
// 
//                // For each row, iterate through all the
//                // columns
//                Iterator<Cell> cellIterator
//                    = row.cellIterator();
// 
//                while (cellIterator.hasNext()) {
// 
//                    Cell cell = cellIterator.next();
// 
//                    // Checking the cell type and format
//                    // accordingly
//                    switch (cell.getCellType()) {
// 
//                    // Case 1
//                    case Cell.CELL_TYPE_NUMERIC:
//                        System.out.print(
//                            cell.getNumericCellValue()
//                            + "t");
//                        break;
// 
//                    // Case 2
//                    case Cell.CELL_TYPE_STRING:
//                        System.out.print(
//                            cell.getStringCellValue()
//                            + "t");
//                        break;
//                    }
//                }
// 
//                System.out.println("");
//            }
// 
//            // Closing file output streams
//            file.close();
//        }
// 
//        // Catch block to handle exceptions
//        catch (Exception e) {
//        	System.out.println(e);
//}
//		return null;
//
//}}