package com.example.protek.controller.readexcelfile;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mysql.cj.result.Row;

@RestController
@RequestMapping("rest/api/excel/")
public class WitreExcelSheet {
	
	
	@PostMapping("write")
public String writeExcelFile() {
		
		//Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook(); 
         
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Employee");
          
        //This data needs to be written (Object[])
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        
        data.put("1", new Object[] {"ID", "NAME", "LASTNAME"});
        data.put("2", new Object[] {1, "Shahruk", "Khan"});
        data.put("3", new Object[] {2, "Waseem", "Shaikh"});
        data.put("4", new Object[] {3, "Israil", "Ali"});
        data.put("5", new Object[] {4, "Farukh", "Pathan"});
          
        //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset)
        {
            XSSFRow row = sheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
               if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        try
        {
        	
        	
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File("C:\\projectDeatils\\Emplyyee_Data_S5.xlsx"));
           
            workbook.write(out);
           
            out.close();
            System.out.println("howtodoinjava_demo.xlsx written successfully on disk.");
        } 
        
        catch (Exception e) 
        {
        	;
            e.printStackTrace();
        }
		return "done +{}";
    }
}


