
package com.example.protek.controller.readexcelfile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.protek.entity.ExcelObject;


@RestController
@RequestMapping("rest/api/post/")
public class WriteExcelSheetByPostman {

	@PostMapping("write")
public String writeExcelFile() throws IOException {
		
		FileOutputStream out = null;
		
		//Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook(); 
         
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Employee");
          
        //This data needs to be written (Object[])
        Map<String, ExcelObject[]> data = new TreeMap<String, ExcelObject[]>();
        
        if(data!=null || data .containsKey(data));{
        	workbook.write(out);
        }
      
          
        //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset)
        {
            XSSFRow row = sheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
               if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        try
        {
        	
        	
            //Write the workbook in file system
             out = new FileOutputStream(new File("C:\\projectDeatils\\Emplyyee_Data_S8.xlsx"));
           
           // workbook.write(out);
           
            out.close();
            System.out.println("howtodoinjava_demo.xlsx written successfully on disk.");
        } 
        
        catch (Exception e) 
        {
        	;
            e.printStackTrace();
        }
		return "done +{}";
    }
}

