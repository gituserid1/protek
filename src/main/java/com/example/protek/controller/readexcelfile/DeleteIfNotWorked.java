package com.example.protek.controller.readexcelfile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("excel/api/")
public class DeleteIfNotWorked {
	
	@GetMapping("excel")
	public ResponseEntity<Object> readExcelValaue() throws InvalidFormatException{

		 try {
			 
			 DeleteIfNotWorked dinw=new DeleteIfNotWorked();
	            //Get the Excel File
//	            FileInputStream file = new FileInputStream(
//	              new File("C:\\projectDeatils\\MonthlyTimeshee.xls"));
	            OPCPackage file = OPCPackage.open(new File("C:\\projectDeatils\\Book1.xlsx"));
	            XSSFWorkbook workbook = new XSSFWorkbook(file);

	            //Get the Desired sheet
	            XSSFSheet sheet = workbook.getSheetAt(0);
                  int id;
                  
	            //Increment over rows
	            for (Row row : sheet) {
	                //Iterate and get the cells from the row
	                Iterator cellIterator = row.cellIterator();
	                // Loop till you read all the data
	                while (cellIterator.hasNext()) {
	                    Cell cell = (Cell) cellIterator.next();

	                    switch (cell.getCellType()) {
	                        case Cell.CELL_TYPE_NUMERIC:
	                        	//return new ResponseEntity<Object>("hi", HttpStatus.OK);
	                           System.out.print(  +cell.getNumericCellValue() + "\t\t\t");
	                            break;
	                        case Cell.CELL_TYPE_STRING:
	                            System.out.print(cell.getStringCellValue() + "\t\t\t");
	                            break;
	                    }
	                }
	                System.out.println(" ");
	            }
	            file.close();
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		return null;
	}

}
