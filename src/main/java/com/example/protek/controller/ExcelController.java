package com.example.protek.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.protek.helper.classs.Helper;
import com.example.protek.service.ExcelServiceNew;

@RestController
@RequestMapping("/rest/api/excel")
public class ExcelController {

	@Autowired
	private ExcelServiceNew esn;

	@PostMapping("/product/upload")
	public ResponseEntity<?> upload(@RequestParam("file") MultipartFile file) {
		if (Helper.checkExcelFormat(file)) {
			// true

			this.esn.save(file);

			//return ResponseEntity.ok("File is uploaded and data is saved to db");
			System.out.println("File is uploaded and data is saved to db");

		}
		
		System.out.println("Pls upload excel file");
		return null;

		
		//return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please upload excel file ");
	}

}
