package com.example.protek.excelreader.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/rest/api/excel/")
public class ExcelController {
	
	@GetMapping("reading")
	public ResponseEntity<Object> readDataFromExcel() throws FileNotFoundException{
	try {
		 
		FileInputStream fis =new FileInputStream(new File("C:\\projectDeatils\\excel.txt.txt"));
		
		
		// /Creating  Workbook instance holding reference to .xlsx file
		XSSFWorkbook xwb =new XSSFWorkbook(fis);
		
		//Get first/desired sheet from the workbook
		XSSFSheet  h=xwb.getSheetAt(0);
		Iterator<Row> itertor= h.iterator();
		while(itertor.hasNext()) 
		System.out.println(itertor.next());{
			
		}
		 
		
	}catch(Exception e) {
		System.out.println("Error!" +e);
	}
	return null;
}
	
	// read the file from only tht txt file nd without the XSSFWebbook Object
  //*working here but not as expected one  first one line getting as o/p
	@GetMapping("reading/file")
	public String readDataFromNotePad() throws ClassNotFoundException {
		String in="";
      try {
    	  File f=new File("C:\\projectDeatils\\excel.txt.txt");
    	  System.out.println("Reading the file from the Notepad " +f.getCanonicalPath());
    	  Scanner sc=new Scanner(f);
           in =sc.nextLine();
           for(int i = 0;i<=in.length();i++) {
        	   System.out.println( "Reading " +i);
           }
    	 // System.out.println(in);
		}catch(Exception e) {
			System.out.println("Error" +e);
		}
		return in;
		
	
}
	// getting the null pointer exceptions
	@GetMapping("reading/file/new")
	public String readDataFromNotePads(String path) throws ClassNotFoundException, FileNotFoundException {
		BufferedReader br=new BufferedReader( new FileReader(new File(path)));
		String filePath ="C:\\projectDeatils\\excel.txt.txt";
		 br.lines().collect(Collectors.joining(System.lineSeparator()));
		 
		try {
		String c=readDataFromNotePads(filePath);
		if(c!=null) {
			System.out.println(c);
		}else
			System.out.println("sorry im getting null value ");
	
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
		//br.lines().collect(Collectors.joining(System.lineSeparator()));
	
		
	}
	//try to print all line word as line by line 
	@GetMapping("reading/file/new/all")
	public Scanner readDataFromNotes()  {
		
		
	try {
		
		//FileInputStream fileinput =new FileInputStream("C:\\projectDeatils\\excel.txt.txt");
		FileInputStream fileinput =new FileInputStream("C:\\projectDeatils\\MonthlyTimeshee.xls");
		Scanner sc =new Scanner(fileinput);
		
		while(sc.hasNextLine()) {
			
			System.out.println(sc.nextLine());
			//return sc;
		}
	}catch(Exception e) {
		System.out.println("Error !"+e);
	}
		
	
		return null;
		
		
		
		
		
	}
}
		
		




