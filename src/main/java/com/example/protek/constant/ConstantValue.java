package com.example.protek.constant;

import org.springframework.http.ResponseEntity;

public final class ConstantValue {
	
	
	//public static final String OK = "Ok";
	
	private static final Double DollerPrice =76.06;
	private static final Double HoldValue =(double) 0;

	public static Double getDollerprice() {
		return DollerPrice;
	}

	public static Double getHoldvalue() {
		return HoldValue;
	}
	
	
	
}
