package com.example.protek;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RequestMapping("rest/api")
public class ProtekApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ProtekApplication.class, args);
		System.out.println("think man logic");
	} 
	@GetMapping("hi") 
	 public String Ans(){
			return "thinking......";
		}
	@Override
	public void run(String... args) throws Exception {
		System.out.println("hi im command_line_runer");
		
	}
	

}
