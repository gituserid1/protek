package com.example.protek.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//@Entity
//@Table(name="excel")
public class ExcelF {
	 
	@Id  
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "customerid")
	private int CustomerId;
	
	@Column(name = "productname")
	private String ProductName;
	
	@Column(name = "city")
	private String City;
	
	@Column(name = "productpart")
	private String ProductPart;
	
	
	@Column(name = "customername")
	private String CustomerName;
	
	
	@Column(name = "actualrate")
	private int ActualRate;
	
	@Column(name = "minusrate")
	private int MinRate;
	
	@Column(name = "rate")
	private int Rate;
	
	@Column(name = "profit")
	private int Profit;
	
	@Column(name = "Interestrate")
	private int InterestRate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCustomerId() {
		return CustomerId;
	}

	public void setCustomerId(int customerId) {
		CustomerId = customerId;
	}

	public String getProductName() {
		return ProductName;
	}

	public void setProductName(String productName) {
		ProductName = productName;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getProductPart() {
		return ProductPart;
	}

	public void setProductPart(String productPart) {
		ProductPart = productPart;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public int getActualRate() {
		return ActualRate;
	}

	public void setActualRate(int actualRate) {
		ActualRate = actualRate;
	}

	public int getMinRate() {
		return MinRate;
	}

	public void setMinRate(int minRate) {
		MinRate = minRate;
	}

	public int getRate() {
		return Rate;
	}

	public void setRate(int rate) {
		Rate = rate;
	}

	public int getProfit() {
		return Profit;
	}

	public void setProfit(int profit) {
		Profit = profit;
	}

	public int getInterestRate() {
		return InterestRate;
	}

	public void setInterestRate(int interestRate) {
		InterestRate = interestRate;
	
	}

	@Override
	public String toString() {
		return "ExcelF [id=" + id + ", CustomerId=" + CustomerId + ", ProductName=" + ProductName + ", City=" + City
				+ ", ProductPart=" + ProductPart + ", CustomerName=" + CustomerName + ", ActualRate=" + ActualRate
				+ ", MinRate=" + MinRate + ", Rate=" + Rate + ", Profit=" + Profit + ", InterestRate=" + InterestRate
				+ "]";
	}

	public ExcelF(Long id, int customerId, String productName, String city, String productPart, String customerName,
			int actualRate, int minRate, int rate, int profit, int interestRate) {
		super();
		this.id = id;
		CustomerId = customerId;
		ProductName = productName;
		City = city;
		ProductPart = productPart;
		CustomerName = customerName;
		ActualRate = actualRate;
		MinRate = minRate;
		Rate = rate;
		Profit = profit;
		InterestRate = interestRate;
	}

	public ExcelF() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
