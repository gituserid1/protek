package com.example.protek.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="price")
public class CustomerPrice {
	
	
	@Id 
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "fixedmontlycharge")
	private Long fixedmontlycharge;
	
	
	@Column(name = "meteredcharge")
	private Long meteredcharge;
	
	@Column(name = "servicetax")
	private Long servicetax;
	
	@Column(name = "totalbil")
	private Long totalbil;
	
	@Column(name = "billno")
	private Long billno;
	
	@Column(name = "customerno")
	private Long customerno;

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFixedmontlycharge() {
		return fixedmontlycharge;
	}

	public void setFixedmontlycharge(Long fixedmontlycharge) {
		this.fixedmontlycharge = fixedmontlycharge;
	}

	public Long getMeteredcharge() {
		return meteredcharge;
	}

	public void setMeteredcharge(Long meteredcharge) {
		this.meteredcharge = meteredcharge;
	}

	public Long getServicetax() {
		return servicetax;
	}

	public void setServicetax(Long servicetax) {
		this.servicetax = servicetax;
	}

	public Long getTotalbil() {
		return totalbil;
	}

	public void setTotalbil(Long totalbil) {
		this.totalbil = totalbil;
	}

	

	public Long getBillno() {
		return billno;
	}

	public void setBillno(Long billno) {
		this.billno = billno;
	}

	public Long getCustomerno() {
		return customerno;
	}

	public void setCustomerno(Long customerno) {
		this.customerno = customerno;
	}

	


}
