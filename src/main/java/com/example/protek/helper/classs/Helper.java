package com.example.protek.helper.classs;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.io.InputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.protek.entity.ExcelF;




public class Helper {
	
	
	 //check that file is of excel type or not
	public static boolean checkExcelFormat(MultipartFile file) {
	String filecheck=file.getContentType();
	
	try {
		if(filecheck.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
			return true;
		}else {
			return false;
		}
			
		
	}catch(Exception e) {
		e.printStackTrace();
	}
		return false;
		
	
		
	}
	
	//convert excel to list of products
	public static List<ExcelF> convertExcelToListOfProduct(InputStream is){
		List<ExcelF> list=new ArrayList<>();
		try {
			
			
		XSSFWorkbook x=new XSSFWorkbook(is);
	XSSFSheet xsheet=x.getSheetAt(0);
			int rowNumber = 0;
		Iterator<Row> irow=	xsheet.iterator();
		
		
		
		 while (irow.hasNext()) {
             Row row = irow.next();

             if (rowNumber == 0) {
                 rowNumber++;
                 continue;
             }

             Iterator<Cell> cells = row.iterator();

             int cid = 0;

             ExcelF p = new ExcelF();

             while (cells.hasNext()) {
                 Cell cell = cells.next();

                 switch (cid) {
                     case 0: 
                         p.setCustomerId((int) cell.getNumericCellValue());
                         break;
                     case 1:
                         p.setProductName(cell.getStringCellValue());
                         break;
                     case 2:
                         p.setCustomerName(cell.getStringCellValue());
                         break;
                     case 3:
                         p.setRate((int) cell.getNumericCellValue());
                         break;
                     case 4:
                    	 p.setCity(cell.getStringCellValue());
                    	 break;
                     case 5:
                    	 p.setInterestRate((int) cell.getNumericCellValue());
                    
                     case 6:
                    	 p.setMinRate((int) cell.getNumericCellValue());
                         break;
                         
                     case 7:
                    	 p.setActualRate((int) cell.getNumericCellValue());
                    	 break;
                     case 8:
                    	 p.setProductPart(cell.getStringCellValue());
                    	 break;
                     case 9:
                    	 p.setProfit((int) cell.getNumericCellValue());
                    	 break;
                     default:
                    	 System.err.println("not woking as porper..");
                 }
                 cid++;

             }

             list.add(p);


         }


		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		return  null;
	}

	
	
	
}
