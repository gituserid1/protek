package com.example.protek.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.protek.entity.ExcelF;
import com.example.protek.helper.classs.Helper;
import com.example.protek.repo.Excelrepo;

@Service
public class ExcelServiceNew {
	
	@Autowired
    private Excelrepo excelRepo;

    public void save(MultipartFile file) {

        try {
            List<ExcelF> products = Helper.convertExcelToListOfProduct(file.getInputStream());
            this.excelRepo.saveAll(products);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
