package com.example.protek.service;



import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.protek.entity.Convert;
import com.example.protek.entity.CustomerDeatils;


@Service
public interface CustomerDeatilsService {

	ResponseEntity<Object> saveD(CustomerDeatils cd) throws CloneNotSupportedException;

	ResponseEntity<Object> getDataByID(Long id);

	ResponseEntity<Object> currencyConveter(Convert converter);

	

	

}
