
CREATE TABLE IF NOT EXISTS address(
  id bigint(20) NOT NULL AUTO_INCREMENT,
  pincode bigint(100) DEFAULT NULL,
  post varchar(100) DEFAULT NULL,
  taluka varchar(40) DEFAULT NULL,
  plotno bigint(40) DEFAULT NULL,
  dist varchar(40) DEFAULT NULL,
  PRIMARY KEY (id)
  ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;





CREATE TABLE IF NOT EXISTS price(
  id bigint(20) NOT NULL AUTO_INCREMENT,
  fixedmontlycharge bigint(100) DEFAULT NULL,
  meteredcharge bigint(100) DEFAULT NULL,
  servicetax bigint(40) DEFAULT NULL,
  totalbil bigint(40) DEFAULT NULL,
  billno bigint(40) DEFAULT NULL,
  
  customerno bigint(100) DEFAULT NULL, 
  PRIMARY KEY (id)
  ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;





CREATE TABLE IF NOT EXISTS customer(
  id bigint(20) NOT NULL AUTO_INCREMENT,
  address_id bigint(100) DEFAULT NULL,
  price_id bigint(100) DEFAULT NULL,
  surname varchar(100) DEFAULT NULL,
  firstname varchar(40) DEFAULT NULL,
  lastname varchar(40) DEFAULT NULL,
  mobile bigint(40) DEFAULT NULL,
  PRIMARY KEY (id),
  
  KEY customer__address(address_id),
  CONSTRAINT customer__address FOREIGN KEY(address_id) REFERENCES address(id),
  
  
  
  
  KEY customer__price(price_id),
  CONSTRAINT customer__price FOREIGN KEY(price_id) REFERENCES price(id)
  ) 
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;